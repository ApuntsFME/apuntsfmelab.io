# Repositori del web

## TODO
 - [ ] Borrar ficheros innecesarios
 - [ ] Añadir acceso al repositorio de LatexFME

## Esborrar l'historial de commits des de git

Esborrar la carpeta `.git` pot portar problemes. Per esborrar l'historial i mantenir el codi:

```
# Check out to a temporary branch:
git checkout --orphan TEMP_BRANCH

# Add all the files:
git add -A

# Commit the changes:
git commit -am "Initial commit"

# Delete the old branch:
git branch -D master

# Rename the temporary branch to master:
git branch -m master

# Finally, force update to our repository:
git push -f origin master

# Track information for the current branch:
git branch --set-upstream-to=origin/master master
```

Sí, és un copy paste.

## Crear més avatars

Si cal incorporar algú nou a l'equip d'ApuntsFME, els actuals estan basats en els que trobareu [aquí](https://www.flaticon.com/packs/kids-avatars). Cal editar el fitxer ```.svg``` (per tal que es vegi amb bona definició), podeu fer-ho amb el programa *Inkscape*.

## Afegir emojis nous

Per tenir emojis utilitzem [Twemoji](https://github.com/twitter/twemoji). Si vols afegir més emojis edita el fitxer `_assets//css/emojis.min.css` i descarrega les imatges a la carpeta corresponent. Si vas prou enrere (Maig 2020), el css esmentat tenia tots els links a tots els emojis (per si t'és útil per veure quins hi ha), però es va canviar per no fer ús d'una CDN (a més de disminuir el tamany del fitxer).

### Més informació
Per més informació, llegir [aquesta web](https://gist.github.com/heiswayi/350e2afda8cece810c0f6116dadbe651l) o [Google](https://www.google.es/).
