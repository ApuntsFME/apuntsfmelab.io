function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
					c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
			}
	}
	return "";
}

function setCookie(cname, cvalue) {
	document.cookie = cname + "=" + cvalue + ";" + ";path=/";
}

function trollMsg() {
	var tt = getCookie("trolled_times");
	tt = parseInt(tt);
	if (tt == 0)
		alert("Ho sentim, aquest link encara no està disponible.");
	if (tt == 1)
		alert("Sento comunicar-te que aquest tampoc, pero hi estem treballant.");
	if (tt == 2)
		alert("Comences a ser una mica pesat amb tant de clic.");
	if (tt == 3)
		alert("De veritat és necessari provar-los tots?");
	if (tt == 4)
		alert("M\'estàs cansat, prova a una altra pestanya.");
	if (tt == 5)
		alert("A veure, cap d\'ou, si no anaven a l\'altra pestanya, tampoc funcionaran aquí, t\'han enganyat.");
	if (tt == 6)
		alert("No et canses mai?");
	if (tt == 7)
		alert("De veritat, fes quelcom útil amb la teva vida.");
	if (tt == 8)
		alert("Escolta, li estàs dedicant més temps a això que a estudiar.");
	if (tt == 9)
		alert("Bé, el que suspendrà ets tu...");
	if (tt == 10)
		alert("Si busques un easter egg, no n'hi ha (pesat)");
	if (tt == 11)
		alert("M'has esgotat el text, ja no hi ha res més.");
	if (tt == 12)
		alert("Que no era broma, no hi ha res més");
	if (tt == 13)
		alert("N O   H I   H A   R E S   M É S!");
	if (tt == 14)
		alert("N O   H I   H A   R E S   M É S!");
	if (tt == 15)
		alert("N O   H I   H A   R E S   M É S!");
	if (tt == 16)
		alert("N O   H I   H A   R E S   M É S!");
	if (tt == 17)
		alert("O potser si...");
	if (tt == 18)
		alert("Hi havia una vegada...");
	if (tt == 19)
		alert("És broma, no t'explicaré un conte (encara no estem tan avorrits)");
	if (tt == 20)
		alert("Això s'està tornant un vici hehe");
	if (tt == 21)
		alert("Com veig que no pares començaré a molestar-te...");
	if (tt == 22)
		alert("Ho dic molt seriosament eeh");
	if (tt == 23)
		alert("De veritat, pel teu propi bé, para ja");
	if (tt == 24)
		alert("Tu t\'ho has buscat. Qui avisa no és traidor.");
	if (tt == 25)
		alert("Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
	if (tt == 26)
		alert("¿Has pogut llegir la frase en català que hi havia entre el llatí?");
	if (tt == 27)
		alert("Doncs et quedes amb la intriga, hauràs de tornar a llegir-ho tot per tornar-hi.");
	if (tt == 28)
		alert("Bé, com no pares (i a més t\'he avisat), començo amb el SPAM, passa-ho bé");
	if (tt == 29)
		alert("Aquí hauríem d\'enviar-te a una web que encara no hem fet hehe");
	if (tt == 30)
		alert("Aquí també");
	if (tt == 31)
		alert("¿Encara no et dónes per vençut?");
	if (tt == 32)
		alert("Doncs va, ho mereixes...");
	if (tt >= 33)
		$(location).attr('href', 'https://www.nyan.cat/')
	setCookie("trolled_times", tt+1);
};

$(document).ready(function(){
	var tt = getCookie("trolled_times");

	if (tt == "") {
		setCookie("trolled_times", 0);
	}
});
