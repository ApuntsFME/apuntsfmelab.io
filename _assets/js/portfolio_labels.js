
jQuery(document).ready(function ($) {

    function resizeText() {
        
        var PortfolioSection = $('.portfolio-section');

        if (PortfolioSection.length) {

            PortfolioSection.each(function () {

                var $this = $(this),
                    figures = $this.find('figure');
                                                
                if (figures.length) {
                    var figureHeight = figures.find('img').height(),
                        textSize = figureHeight * 0.14;
                    
                    figures.each(function () {
                        var captionDiv = $this.find('.caption');
                        captionDiv.css({
                            'font-size': textSize
                        });
                    });
                }

            });

        }

    }

    resizeText();
    $(window).on('load', resizeText);
    $(window).on('resize', resizeText);

});
